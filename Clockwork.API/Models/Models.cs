﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Clockwork.API.Models
{
    public class ClockworkContext : DbContext
    {
        public DbSet<CurrentTimeQuery> CurrentTimeQueries { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=clockwork.db");
        }
    }

    public class CurrentTimeQuery
    {
        [Key]
        public int CurrentTimeQueryId { get; set; }
        public DateTimeOffset Time { get; set; }
        public string ClientIp { get; set; }
        public DateTime UTCTime { get; set; }
        public string TimeZone { get; set; }
    }
    public class CurrentTimeResponse
    {
        public int CurrentTimeQueryId { get; set; }
        public string Time { get; set; }
        public string ClientIp { get; set; }
        public string UTCTime { get; set; }
        public string TimeZone { get; set; }

        public CurrentTimeResponse(CurrentTimeQuery query)
        {
            this.CurrentTimeQueryId = query.CurrentTimeQueryId;
            this.Time = query.Time.ToString("MM/dd/yyyy HH:mm:ss");
            this.ClientIp = query.ClientIp;
            this.UTCTime = query.UTCTime.ToString("MM/dd/yyyy HH:mm:ss");
            this.TimeZone = TimeZoneInfo.FindSystemTimeZoneById(query.TimeZone).DisplayName;
        }
    }

    public class TimeZoneResponse 
    {
        public string Id;
        public string DisplayName;
        
        public TimeZoneResponse(TimeZoneInfo info)
        {
            this.Id = info.Id;
            this.DisplayName = info.DisplayName;
        }

    }
}
