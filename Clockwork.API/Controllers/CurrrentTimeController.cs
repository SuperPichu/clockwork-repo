﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Clockwork.API.Controllers
{
    [Route("api/time")]
    public class CurrentTimeController : Controller
    {
        // GET api/time/current
        [HttpGet]
        [Route("current/{zoneId}")]
        public IActionResult Get(string zoneId)
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(utcTime, zoneId);
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();

            var returnVal = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime,
                TimeZone = zoneId
            };

            using (var db = new ClockworkContext())
            {
                db.CurrentTimeQueries.Add(returnVal);
                var count = db.SaveChanges();
                Console.WriteLine("{0} records saved to database", count);

                Console.WriteLine();
                foreach (var CurrentTimeQuery in db.CurrentTimeQueries)
                {
                    Console.WriteLine(" - {0}", CurrentTimeQuery.UTCTime);
                }
            }

            return Ok(new CurrentTimeResponse(returnVal));
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetList()
        {
            using (var db = new ClockworkContext())
            {
                return Ok(db.CurrentTimeQueries.Select(c => new CurrentTimeResponse(c)).ToArray());
            }
        }

        [HttpGet]
        [Route("zones")]
        public IActionResult GetZones()
        {
            List<TimeZoneResponse> responses = new List<TimeZoneResponse>();
            foreach(TimeZoneInfo info in TimeZoneInfo.GetSystemTimeZones())
            {
                responses.Add(new TimeZoneResponse(info));
            }

            return Ok(responses.ToArray());
        }
    }
}
