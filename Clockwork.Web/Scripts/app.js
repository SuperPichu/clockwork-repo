﻿function GetTime() {
    var select = document.getElementById("zones");
    var zone = select[select.selectedIndex]["value"];
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var respObj = JSON.parse(this.responseText);
            document.getElementById("output").innerText = "Current Time: " + respObj["time"];
            AddLine(respObj);
        }
    };
    xhttp.open("GET", "http://127.0.0.1:5000/api/time/current/" + zone, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();

}

function GetList() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var respArray = JSON.parse(this.responseText);
            for (var i = 0; i < respArray.length; i++) {
                var respObj = respArray[i];
                AddLine(respObj);
            }
        }
    };
    xhttp.open("GET", "http://127.0.0.1:5000/api/time/all", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}

function GetZones() {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var respArray = JSON.parse(this.responseText);
            for (var i = 0; i < respArray.length; i++) {
                var respObj = respArray[i];
                AddZone(respObj);
            }
        }
    };
    xhttp.open("GET", "http://127.0.0.1:5000/api/time/zones", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}

function AddZone(zone) {
    var item = document.createElement("option");
    item.value = zone["id"];
    item.innerText = zone["displayName"];
    document.getElementById("zones").appendChild(item);

}

function AddLine(line) {
    var item = document.createElement("tr");
    var id = document.createElement("td");
    id.innerText = line["currentTimeQueryId"];
    item.appendChild(id);
    var ip = document.createElement("td");
    ip.innerText = line["clientIp"];
    item.appendChild(ip);
    var time = document.createElement("td");
    time.innerText = line["time"];
    item.appendChild(time);
    var zone = document.createElement("td");
    zone.innerText = line["timeZone"];
    item.appendChild(zone);
    var utc = document.createElement("td");
    utc.innerText = line["utcTime"];
    item.appendChild(utc);
    document.getElementById("list").appendChild(item);
}
GetZones();
GetList();